import matplotlib.pyplot as plt
import numpy as np
import glob                        # use linux wildcard syntax
import numpy as np                 # python numeric library
import pandas as pd                # create excel like data frames

# The origin directories with respective files, changes this if your name or directory differs, don't forget to change the plot commands!
data_Closed =     pd.read_csv("fractures/closedsystem/blackoil/"    + "masses.dat" ,delim_whitespace=False)
data_TopOpen =    pd.read_csv("fractures/topopensystem/blackoil/"   + "masses.dat" ,delim_whitespace=False)
data_BottomJect = pd.read_csv("fractures/bottominjection/blackoil/" + "masses.dat" ,delim_whitespace=False)

tsz=17
fig = plt.figure(figsize=(20,7 ))

ax1=fig.add_axes([.1,.1, .8, .8])
ax1.grid(True)
ax1.tick_params(axis='both', which='major', labelsize=tsz)

plt.title(r'Time Evolution of Total Masses Inside the Modelling Domain; Fractured System.')
plt.xlabel('simulation time [s]', fontsize = tsz)
plt.ylabel('mass [kg/m]',fontsize = tsz)

# "Mat", for matrix subdomain
# "Frc", for fractured subdomain.

#Plots, comment or uncomment with respecte to what the plot should show.

# Total Domain
#moG
ax1.plot(data_Closed.time, data_Closed.Mat_moG+  data_Closed.Frc_moG   ,   '-', markersize=1, label="moG; Quasi Closed System" )
ax1.plot(data_TopOpen.time, data_TopOpen.Mat_moG +  data_TopOpen.Frc_moG ,   '-', markersize=1, label="moG; Top Open System" )
ax1.plot(data_BottomJect.time, data_BottomJect.Mat_moG  +data_BottomJect.Frc_moG    ,   '-', markersize=1, label="moG; Bottom Injection System" )

#Mat_moG
#ax1.plot(data_Closed.time, data_Closed.Mat_moG    ,   '-', markersize=1, label="Mat_moG; Quasi Closed System" )
#ax1.plot(data_TopOpen.time, data_TopOpen.Mat_moG   ,   '-', markersize=1, label="Mat_moG; Top Open System" )
#ax1.plot(data_BottomJect.time, data_BottomJect.Mat_moG   ,   '-', markersize=1, label="Mat_moG; Bottom Injection System" )
##Frc_moG
#ax1.plot(data_Closed.time, data_Closed.Frc_moG    ,   '-', markersize=1, label="Frc_moG; Quasi Closed System" )
#ax1.plot(data_TopOpen.time, data_TopOpen.Frc_moG   ,   '-', markersize=1, label="Frc_moG; Top Open System" )
#ax1.plot(data_BottomJect.time, data_BottomJect.Frc_moG   ,   '-', markersize=1, label="Frc_moG; Bottom Injection System" )

# Frc_mO
#ax1.plot(data_Closed.time, data_Closed.Frc_mO    ,   '-', markersize=1, label="Frc_mO; Quasi Closed System" )
#ax1.plot(data_TopOpen.time, data_TopOpen.Frc_mO   ,   '-', markersize=1, label="Frc_mO; Top Open System" )
#ax1.plot(data_BottomJect.time, data_BottomJect.Frc_mO   ,   '-', markersize=1, label="Frc_mO; Bottom Injection System" )
# Mat_mO
#ax1.plot(data_Closed.time, data_Closed.Mat_mO    ,   '-', markersize=1, label="Mat_mO; Quasi Closed System" )
#ax1.plot(data_TopOpen.time, data_TopOpen.Mat_mO   ,   '-', markersize=1, label="Mat_mO; Top Open System" )
#ax1.plot(data_BottomJect.time, data_BottomJect.Mat_mO   ,   '-', markersize=1, label="Mat_mO; Bottom Injection System" )

#Mat_mW
#ax1.plot(data_Closed.time, data_Closed.Mat_mW    ,   '-', markersize=1, label="Mat_mW; Quasi Closed System" )
#ax1.plot(data_TopOpen.time, data_TopOpen.Mat_mW   ,   '-', markersize=1, label="Mat_mW; Top Open System" )
#ax1.plot(data_BottomJect.time, data_BottomJect.Mat_mW   ,   '-', markersize=1, label="Mat_mW; Bottom Injection System" )
#Frc_mW
#ax1.plot(data_Closed.time, data_Closed.Frc_mW    ,   '-', markersize=1, label="Frc_mW; Quasi Closed System" )
#ax1.plot(data_TopOpen.time, data_TopOpen.Frc_mW   ,   '-', markersize=1, label="Frc_mW; Top Open System" )
#ax1.plot(data_BottomJect.time, data_BottomJect.Frc_mW   ,   '-', markersize=1, label="Frc_mW; Bottom Injection System" )

# Frc_mgG
#ax1.plot(data_Closed.time, data_Closed.Frc_mgG    ,   '-', markersize=1, label="Frc_mgG; Quasi Closed System" )
#ax1.plot(data_TopOpen.time, data_TopOpen.Frc_mgG   ,   '-', markersize=1, label="Frc_mgG; Top Open System" )
#ax1.plot(data_BottomJect.time, data_BottomJect.Frc_mgG   ,   '-', markersize=1, label="Frc_mgG; Bottom Injection System" )
# Mat_mgG
#ax1.plot(data_Closed.time, data_Closed.Mat_mgG    ,   '-', markersize=1, label="Mat_mgG; Quasi Closed System" )
#ax1.plot(data_TopOpen.time, data_TopOpen.Mat_mgG   ,   '-', markersize=1, label="Mat_mgG; Top Open System" )
#ax1.plot(data_BottomJect.time, data_BottomJect.Mat_mgG   ,   '-', markersize=1, label="Mat_mgG; Bottom Injection System" )

# Marking the injection stop
ax1.plot([14400,14400],[100,0],':',color='firebrick' ) #80


plt.legend(fontsize = tsz-6)
plt.tight_layout()
plt.show()
