// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BlackOilTests
 * \brief
 */
#include <config.h>
#include <cmath>
#include <vector>
// #include <algorithm>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/linear/linearsolvertraits.hh>
#if CHECKRELATIVESHIFTREDUCTION==0
#include <dumux/multidomain/newtonsolver.hh>
#elif CHECKRELATIVESHIFTREDUCTION==1
#include <mynewtonsolver/newtonsolverwithreductioncheking.hh>
#endif

#include <dumux/assembly/fvassembler.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager_yasp.hh>
#include <dumux/io/loadsolution.hh>
#include "properties.hh"

int main(int argc, char** argv)
{

    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::TYPETAG;

    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);
//     gridGeometry->update();

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");
    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = 0;
    if (Parameters::getTree().hasKey("Restart.Restart"))
        restartTime = getParam<Scalar>("Restart.Restart");

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(gridGeometry->numDofs());
    problem->applyInitialSolution(x);

    // Restart the simulation, if we have given a specific time, be careful that everything matches!
    if (restartTime > 0)
    {
        using IOFields = GetPropType<TypeTag, Properties::IOFields>;
        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        const auto fileName = getParam<std::string>("Restart.File");
        const auto pvName = createPVNameFunction<IOFields, PrimaryVariables, ModelTraits, FluidSystem>();
        loadSolution(x, fileName, pvName, *gridGeometry);
    }
    else
        problem->applyInitialSolution(x);


    auto xOld = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    bool enableVtkOutput = getParam<bool>("Problem.EnableVtkOutput",true);
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    vtkWriter.addVelocityOutput(std::make_shared<VelocityOutput>(*gridVariables));
    IOFields::initOutputModule(vtkWriter); // Add model specific output fields
    if(enableVtkOutput)
        vtkWriter.write(0.0);

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);
    timeLoop->setPeriodicCheckPoint(getParam<Scalar>("TimeLoop.EpisodeLength", std::numeric_limits<Scalar>::max()));

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables, timeLoop, xOld);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);


    // extra output files, gives every timesteps information
    static unsigned int outputPrecision = 12;
    auto csvFileName = getParam<std::string>("Problem.OutputfileMasses");
    // Only denote divRsBy if RsDivisor is not 1.0
    if(getParam<Scalar>("Problem.RsDivisor",1.0) != 1.0)
    {
        csvFileName = csvFileName + "_divRsBy" +  std::to_string(getParam<Scalar>("Problem.RsDivisor",1.0));
    }
    csvFileName = csvFileName + ".dat";
    std::ofstream massesOutputFile;
    massesOutputFile.open(csvFileName, std::ios::out);
    massesOutputFile << std::scientific << std::setprecision(outputPrecision);

    // Get postprocessing information
    // Initial Values
    const auto massData = problem->getPostProcessingData(*gridVariables, x);
    // Headline in csv file
    massesOutputFile << "time,";
    for(int ii =0;ii<massData.size()-1;++ii)
    {
        massesOutputFile<< massData[ii].first <<",";
    }
    massesOutputFile << massData.back().first <<std::endl;
    // write data into file
    massesOutputFile << timeLoop->time() <<",";
    for (int ii=0; ii<massData.size() -1;++ii)
    {
        massesOutputFile << massData[ii].second << ",";
    }
    massesOutputFile << massData.back().second << std::endl;// Finishing a row!
    // Statistical parameters to check the "closeness" of the system
    double summationsMasses=massData.back().second; // This is x_0 for the arithmetic mean estimation!
    std::vector<double> massAtTimeStep;
    massAtTimeStep.emplace_back(massData.back().second);
    double arithmeticMean;
    double summationsVariances, stdDeviation;


    // time loop
    timeLoop->start();
    while (!timeLoop->finished())
    {
        // solve the non-linear system with time step control
        nonLinearSolver.solve(x, *timeLoop);



        // make the new solution the old solution
        xOld = x;
        gridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        // if episode length was specificied output only at the end of episodes
        if (!hasParam("TimeLoop.EpisodeLength") || timeLoop->isCheckPoint() || timeLoop->finished() || timeLoop->timeStepIndex() == 1)
        {
            if(enableVtkOutput)
                vtkWriter.write(timeLoop->time());
        }

        // Get postprocessing information
        const auto massData = problem->getPostProcessingData(*gridVariables, x);
        // write data into file
        massesOutputFile << timeLoop->time() <<",";
        for (int ii=0; ii<massData.size() -1;++ii)
        {
            massesOutputFile << massData[ii].second << ",";
        }
        massesOutputFile << massData.back().second << std::endl;// Finishing a row!

        // Statistics for checking system closedness
        massAtTimeStep.emplace_back(massData.back().second );
        summationsMasses += massData.back().second;
        arithmeticMean = summationsMasses/((double)timeLoop->timeStepIndex() +1); // first is initial conditions, so we have numTimeSteps + 1 data points!
        summationsVariances=0.0;
        for (int i =0; i<massAtTimeStep.size();i++)
        {
            summationsVariances += (massAtTimeStep[i] - arithmeticMean)*(massAtTimeStep[i] - arithmeticMean);
        }
        stdDeviation = std::sqrt(summationsVariances)/((double)timeLoop->timeStepIndex());
        std::cout << std::setprecision(18) << "Total Mass arithmetic mean = " << arithmeticMean << " Standard deviation = " << stdDeviation << std::endl;
        // Now some statistical checks
        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));
    }

    timeLoop->finalize(leafGridView.comm());
    massesOutputFile.close();

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;

}
