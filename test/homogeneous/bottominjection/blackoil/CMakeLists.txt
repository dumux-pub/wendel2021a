add_input_file_links(FILES params.input, So72_HomogeneousBottomInjectionSystem.input, So74_HomogeneousBottomInjectionSystem.input)
# isothermal tests
dumux_add_test(NAME bottominjection_box
              LABELS porousmediumflow blackoil
              SOURCES main.cc
              COMPILE_DEFINITIONS TYPETAG=BottomInjectionBlackOilBox
              COMPILE_DEFINITIONS CHECKRELATIVESHIFTREDUCTION=0
              COMMAND ${CMAKE_SOURCE_DIR}/bin/testing/runtest.py)

dumux_add_test(NAME bottominjection_box_checkrelshiftreduction
              LABELS porousmediumflow blackoil
              SOURCES main.cc
              COMPILE_DEFINITIONS TYPETAG=BottomInjectionBlackOilBox
              COMPILE_DEFINITIONS CHECKRELATIVESHIFTREDUCTION=1
              COMMAND ${CMAKE_SOURCE_DIR}/bin/testing/runtest.py)

dumux_add_test(NAME bottominjection_tpfa
              LABELS porousmediumflow blackoil
              SOURCES main.cc
              COMPILE_DEFINITIONS TYPETAG=BottomInjectionBlackOilCCTpfa
              COMPILE_DEFINITIONS CHECKRELATIVESHIFTREDUCTION=0
              COMMAND ${CMAKE_SOURCE_DIR}/bin/testing/runtest.py)

dumux_add_test(NAME bottominjection_tpfa_checkrelshiftreduction
              LABELS porousmediumflow blackoil
              SOURCES main.cc
              COMPILE_DEFINITIONS TYPETAG=BottomInjectionBlackOilCCTpfa
              COMPILE_DEFINITIONS CHECKRELATIVESHIFTREDUCTION=1
              COMMAND ${CMAKE_SOURCE_DIR}/bin/testing/runtest.py)
