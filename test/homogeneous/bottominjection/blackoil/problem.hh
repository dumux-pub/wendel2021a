// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BlackOilProblems
 */

#ifndef DUMUX_BOTTOMINJECTION_BLACKOIL_PROBLEM_HH
#define DUMUX_BOTTOMINJECTION_BLACKOIL_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>

#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

/*!
 * \ingroup BlackOilTests
 * \brief Isothermal Black Oil Tests System.
 *
 * The 2D domain of this test problem is 3 km long and 4 km high.
 *
 * The top of the borders receives a Dirichlet boundary condition.
 * Left and right boundaries receive Neumann no flow.
 * Bottom boundary receives water influx condition as a Neumann boundary condition.
 *
 * This problem uses the \ref BlackOilModel
 *
 *  */
template <class TypeTag >
class BottomInjectionBlackOilProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Indices = BlackOilIndices;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    static constexpr int numComponents =3;
    static constexpr int numPhases = 3;
    // copy some indices for convenience
    enum {
        pressureIdx = Indices::pressureIdx,
        oSaturationIdx = Indices::oSaturationIdx,
        wSaturationIdx = Indices::wSaturationIdx,

        // world dimension
        dimWorld = GridView::dimensionworld
    };
    static constexpr int wPhaseIdx = FluidSystem::wPhaseIdx;
    static constexpr int gPhaseIdx = FluidSystem::gPhaseIdx;
    static constexpr int oPhaseIdx = FluidSystem::oPhaseIdx;
    static constexpr int oCompIdx = Indices::conti0EqIdx + FluidSystem::oCompIdx; //!< Index of the mass conservation equation for the oil component
    static constexpr int wCompIdx = Indices::conti0EqIdx + FluidSystem::wCompIdx;//!< Index of the mass conservation equation for the water componen
    static constexpr int gCompIdx = Indices::conti0EqIdx + FluidSystem::gCompIdx;//!< Index of the mass conservation equation for the gas component
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    BottomInjectionBlackOilProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        // get injection Rate
        injectionRate_ = getParam<Scalar>("Problem.InjectionRate");
        FluidSystem::initBegin();
        // NOTE: Bg has a quasi unsteadiness at pressures below 2.7e6
        std::vector<std::pair<Scalar, Scalar> > Bg = {
            { 1.013529e+05, 9.998450e-01  },
            { 2757902.8,    0.03312601837829474 },
            { 5515805.6,    0.01656300918914737 },
            { 8273708.4,    0.011004575596857235 },
            { 11031611.2,   0.008253431697642926 },
            { 13789514.0,   0.006625203675658949 },
            { 16547416.8,   0.005502287798428617 },
            { 19305319.6,   0.004716246684367386 },
            { 22063222.4,   0.004154788745752222 },
            { 24821125.2,   0.0036494766009985734 },
            { 27579028.0,   0.0033126018378294743 },
            { 30336930.8,   0.003031872868521892 },
            { 33094833.6,   0.0027511438992143086 },
            { 35852736.4,   0.002526560723768243 },
            { 38610639.2,   0.002358123342183693 }
        };
        std::vector<std::pair<Scalar, Scalar> > Bo = {
            { 1.013529e+05, 1.000000e+00  },
            { 2757902.8,    1.012  },
            { 5515805.6,    1.0255 },
            { 8273708.4,    1.038  },
            { 11031611.2,   1.051  },
            { 13789514.0,   1.063  },
            { 16547416.8,   1.075  },
            { 19305319.6,   1.087  },
            { 22063222.4,   1.0985 },
            { 24821125.2,   1.11   },
            { 27579028.0,   1.12   },
            { 30336930.8,   1.13   },
            { 33094833.6,   1.14   },
            { 35852736.4,   1.148  },
            { 38610639.2,   1.155  }
        };
        // TEST: Division of all Rs spline values by a runtime factor, WARNING: This is a hack and not very physical!!!
        rsDivisor_ = getParam<Scalar>("Problem.RsDivisor",1.0);
        std::cout << "Rs Divisor is set to "<< rsDivisor_<<std::endl;
        std::vector<std::pair<Scalar, Scalar> > Rs = {
            { 1.013529e+05, 0.000000e+00 },
            { 2757902.8,    29.387772648867333/rsDivisor_ },
            { 5515805.6,    59.666083862851856/rsDivisor_ },
            { 8273708.4,    89.05385651171919 /rsDivisor_ },
            { 11031611.2,   118.44162916058653/rsDivisor_ },
            { 13789514.0,   147.47318638340698/rsDivisor_ },
            { 16547416.8,   175.43609732808682/rsDivisor_ },
            { 19305319.6,   201.26171571648536/rsDivisor_ },
            { 22063222.4,   226.19679553976673/rsDivisor_ },
            { 24821125.2,   247.56972110257934/rsDivisor_ },
            { 27579028.0,   267.1615695351575 /rsDivisor_},
            { 30336930.8,   284.9723408375014 /rsDivisor_},
            { 33094833.6,   298.5085270272827 /rsDivisor_},
            { 35852736.4,   311.6884977910172 /rsDivisor_},
            { 38610639.2,   322.37496057242345/rsDivisor_ }
        };
        std::vector<std::pair<Scalar, Scalar> > muo = {
            { 1.013529e+05,  1.200000e-03 },
            { 2757902.8,     0.00117},
            { 5515805.6,     0.00114},
            { 8273708.4,     0.00111},
            { 11031611.2,    0.00108},
            { 13789514.0,    0.00106},
            { 16547416.8,    0.00103},
            { 19305319.6,    0.00100},
            { 22063222.4,    0.00098},
            { 24821125.2,    0.00095},
            { 27579028.0,    0.00094},
            { 30336930.8,    0.00092},
            { 33094833.6,    0.00091},
            { 35852736.4,    0.00090},
            { 38610639.2,    0.00089}
        };
        std::vector<std::pair<Scalar, Scalar> > mug = {
            { 1.013529e+05, 1.2500e-05   },
            { 2757902.8,    1.3000e-05   },
            { 5515805.6,    1.3500e-05   },
            { 8273708.4,    1.4000e-05   },
            { 11031611.2,   1.4500e-05   },
            { 13789514.0,   1.5000e-05   },
            { 16547416.8,   1.5500e-05   },
            { 19305319.6,   1.6000e-05   },
            { 22063222.4,   1.6500e-05   },
            { 24821125.2,   1.7000e-05   },
            { 27579028.0,   1.7500e-05   },
            { 30336930.8,   1.8000e-05   },
            { 33094833.6,   1.8500e-05   },
            { 35852736.4,   1.9000e-05   },
            { 38610639.2,   1.9500e-05   }
        };
        FluidSystem::setGasFormationVolumeFactor(Bg);
        FluidSystem::setOilFormationVolumeFactor(Bo);
        FluidSystem::setGasFormationFactor(Rs);
        FluidSystem::setOilViscosity(muo);
        FluidSystem::setGasViscosity(mug);
        FluidSystem::setWaterViscosity(9.6e-4);
        FluidSystem::setWaterCompressibility(1.450377e-10);
        FluidSystem::setSurfaceDensities(/*oil=*/720.51,
                                         /*water=*/1009.32,
                                         /*gas=*/1.1245);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++ phaseIdx)
            FluidSystem::setReferenceVolumeFactor(phaseIdx, 1.0);
        FluidSystem::initEnd();
        extrusionFactor_ = getParam<Scalar>("Problem.ExtrusionFactor");
        // Get Initial conditions
        upperOilSaturation_ = getParam<Scalar>("InitialConditions.UpperOilSaturation");
        upperWaterSaturation_ = getParam<Scalar>("InitialConditions.UpperWaterSaturation");
        topGasPressure_ = getParam<Scalar>("InitialConditions.TopGasPressure");
        waterOilContact_ = getParam<Scalar>("InitialConditions.WaterOilContact");
        lowerOilSaturation_= getParam<Scalar>("InitialConditions.LowerOilSaturation");
        lowerWaterSaturation_= getParam<Scalar>("InitialConditions.LowerWaterSaturation");
        // injection specifications
        injectionRate_ = getParam<Scalar>("Problem.InjectionRate");
        injectionPosition_ = getParam<Scalar>("Problem.InjectionPosition");
        injectionLength_ = getParam<Scalar>("Problem.InjectionLength");
        name_ = name_ + getParam<std::string>("Problem.Name");
        // We also want to add Discretization method and model divisor.
        static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;
        static constexpr bool isCCTPFA = GridGeometry::discMethod == DiscretizationMethod::cctpfa;
        if(isBox)
            name_ = name_ + "_box";
        else if(isCCTPFA)
            name_ = name_ + "_cctpfa";
        else
            name_ = name_ + "_";
        // If the solution GOR hack is applied, it will be denoted in the problem name.
        if(rsDivisor_ != 1)
        {
            name_ = name_ + "_divRsBy" +  std::to_string(rsDivisor_);
        }
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * \param globalPos The global position
     *
     * The black-oil model assumes constant temperature to define its
     * parameters. Although temperature is thus not really used by the
     * model, it gets written to the VTK output. Who nows, maybe we
     * will need it one day?
     */
    Scalar temperatureAtPos(const GlobalPosition &globalPos) const
    {
        return temperature_;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        const auto& yMax = this->gridGeometry().bBoxMax()[1]-eps_;
        if(globalPos[1] > (yMax)  )
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        return initial_(globalPos);
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);

        // negative values for injection
        if (globalPos[1] < 1e-6
            && globalPos[0] > injectionPosition_
            && globalPos[0] < injectionPosition_ + injectionLength_)
        {
            values[oCompIdx] = 0.0;
            values[wCompIdx] = injectionRate_;
            values[gCompIdx] = 0.0;
        }

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return initial_(globalPos); }
    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    {
        return extrusionFactor_;
    }
    //! computes the influx of water into the domain by injection (neumann BC)
    Scalar computeInjectionFlux() const
    {
        Scalar flux = 0.0;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bind(element);

            // As we only inject water component, we only return a Scalar value
            for (const auto& scvf : scvfs(fvGeometry))
                if (scvf.boundary())
                    flux -= neumannAtPos(scvf.ipGlobal())[wCompIdx]*scvf.area();
        }

        return flux;
    }
    //! Post processing the masses of the system
    template<class GridVariables, class SolutionVector>
    std::vector<std::pair<std::string, Scalar>> getPostProcessingData(const GridVariables& gridVars,
                                 const SolutionVector& x) const
    {
        using DataPair = std::pair<std::string, Scalar>;
        using Result = std::vector<DataPair>;
        Result masses;
        int i = 0;
        int idx_moG = i++;          masses.emplace_back( std::make_pair("moG", 0.0) );
        int idx_mgG = i++;          masses.emplace_back( std::make_pair("mgG", 0.0) );
        int idx_mwW = i++;          masses.emplace_back( std::make_pair("mwW", 0.0) );
        int idx_moO = i++;          masses.emplace_back( std::make_pair("moO", 0.0) );
        int idx_mG  = i++;          masses.emplace_back( std::make_pair("mG" , 0.0) );
        int idx_mO  = i++;          masses.emplace_back( std::make_pair("mO" , 0.0) );
        int idx_mW  = i++;          masses.emplace_back( std::make_pair("mW" , 0.0) );
        int idx_mTot = i++;         masses.emplace_back( std::make_pair("mTot", 0.0) );


        Scalar mass_oG = 0.0;
        Scalar mass_gG = 0.0;
        Scalar mass_wW = 0.0;
        Scalar mass_oO = 0.0;
        Scalar mass_G  = 0.0;
        Scalar mass_O  = 0.0;
        Scalar mass_W  = 0.0;
        Scalar massTot = 0.0;
        for( const auto& element : elements(this->gridGeometry().gridView()) )
        {
            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                const Scalar poro = volVars.porosity();
                const Scalar So = volVars.saturation(oPhaseIdx);
                const Scalar Sg = volVars.saturation(gPhaseIdx);
                const Scalar Sw = volVars.saturation(wPhaseIdx);
                const Scalar XwW = volVars.massFraction(wPhaseIdx,wCompIdx);
                const Scalar XoO = volVars.massFraction(oPhaseIdx,oCompIdx);
                const Scalar XoG = volVars.massFraction(oPhaseIdx,gCompIdx);
                const Scalar XgG = volVars.massFraction(gPhaseIdx,gCompIdx);
                const Scalar rhog = volVars.density(gPhaseIdx);
                const Scalar rhoo = volVars.density(oPhaseIdx);
                const Scalar rhow = volVars.density(wPhaseIdx);
                const Scalar vol = elemVolVars[scv].extrusionFactor()*scv.volume();
                mass_oG += XoG*rhoo*So*vol*poro;
                mass_gG += XgG*rhog*Sg*vol*poro;
                mass_wW += XwW*rhow*Sw*vol*poro;
                mass_oO += XoO*rhoo*So*vol*poro;
                // Get total component masses
                for (int PhIdx=0; PhIdx < numPhases; PhIdx++)
                {
                    mass_G += volVars.saturation(PhIdx)*volVars.density(PhIdx)*volVars.massFraction(PhIdx, gCompIdx)*vol*poro;
                    mass_O += volVars.saturation(PhIdx)*volVars.density(PhIdx)*volVars.massFraction(PhIdx,oCompIdx )*vol*poro;
                    mass_W += volVars.saturation(PhIdx)*volVars.density(PhIdx)*volVars.massFraction(PhIdx, wCompIdx )*vol*poro;
                }
            }
        }
        masses[idx_moG].second = mass_oG;
        masses[idx_mgG].second = mass_gG;
        masses[idx_mwW].second = mass_wW;
        masses[idx_moO].second = mass_oO;
        masses[idx_mG].second = mass_G;
        masses[idx_mO].second = mass_O;
        masses[idx_mW].second = mass_W;
        massTot = mass_G+mass_O+mass_W;
        masses[idx_mTot].second = massTot;
        return masses;
    }
private:
    // internal method for the initial condition
    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        const auto& yMax = this->gridGeometry().bBoxMax()[1];
        if(globalPos[1]>waterOilContact_)
        {
            values[pressureIdx] = topGasPressure_+(yMax -globalPos[1])*9.81*1000;
            values[oSaturationIdx] = upperOilSaturation_;
            values[wSaturationIdx] = upperWaterSaturation_;
        }
        else
        {
            values[pressureIdx] = topGasPressure_+(yMax -globalPos[1])*9.81*1000;
            values[oSaturationIdx] = lowerOilSaturation_;
            values[wSaturationIdx] = lowerWaterSaturation_;
        }

        return values;
    }

    template<class FluidMatrixInteraction>
    static Scalar invertPcgw_(Scalar pcIn, const FluidMatrixInteraction& fluidmatrixinteraction)
    {
        Scalar lower,upper;
        int k;
        int maxIt = 50;
        Scalar bisLimit = 1.;
        Scalar sw, pcgw;
        lower=0.0; upper=1.0;
        for (k=1; k<=25; k++)
        {
            sw = 0.5*(upper+lower);
            pcgw = fluidmatrixinteraction.pcgw(sw, 0.0/*sn*/);
            Scalar delta = pcgw-pcIn;
            if (delta<0.) delta*=-1.;
            if (delta<bisLimit)
            {
                return(sw);
            }
            if (k==maxIt) {
                return(sw);
            }
            if (pcgw>pcIn) lower=sw;
            else upper=sw;
        }
        return(sw);
    }
    static constexpr Scalar temperature_ = 311.0; // Only a dummy value, has no effects on simulation.
    Scalar rsDivisor_;
    Scalar extrusionFactor_;
    // convenience variables
    Scalar upperOilSaturation_,upperWaterSaturation_ ,waterOilContact_, lowerOilSaturation_,lowerWaterSaturation_;    // injection specifics
    Scalar injectionRate_;
    Scalar injectionPosition_;
    Scalar injectionLength_;
    Scalar injectionDuration_;
    Scalar topGasPressure_;
    static constexpr Scalar yBd = 40.0;
    static constexpr Scalar eps_ = 1e-6;
    std::string name_;

};

} //end namespace Dumux

#endif
