import matplotlib.pyplot as plt
import numpy as np
import glob                        # use linux wildcard syntax
import numpy as np                 # python numeric library
import pandas as pd                # create excel like data frames

data_Closed =     pd.read_csv("fractures/closedsystem/blackoil/"     + "escapedGas.dat",delim_whitespace=True)
data_TopOpen =    pd.read_csv("fractures/topopensystem/blackoil/"    + "escapedGas.dat",delim_whitespace=True)
data_BottomJect = pd.read_csv("fractures/bottominjection/blackoil/"  + "escapedGas.dat",delim_whitespace=True)

# plot the timestep size over time step
tsz=17
fig = plt.figure(figsize=(20,7 ))

ax1=fig.add_axes([.1,.1, .8, .8])
ax1.grid(True)
ax1.tick_params(axis='both', which='major', labelsize=tsz)

plt.title(r'Time Evolution of Escaped Gas.' )
plt.xlabel('simulation time [s]', fontsize = tsz)
plt.ylabel('mass [kg/m]',fontsize = tsz)


ax1.plot(data_Closed.time, data_Closed.EscapeGas   ,   '-', markersize=1, label="Quasi Closed System" )
ax1.plot(data_TopOpen.time, data_TopOpen.EscapeGas   ,   '-', markersize=1, label="Top Open System" )
ax1.plot(data_BottomJect.time, data_BottomJect.EscapeGas   ,   '-', markersize=1, label="Bottom Injection System" )
# Marking the injection stop
ax1.plot([14400,14400],[250,0],':',color='firebrick' )


plt.legend(fontsize = tsz-6)
plt.tight_layout()
plt.show()
