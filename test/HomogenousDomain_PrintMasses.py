import matplotlib.pyplot as plt
import numpy as np
import glob                        # use linux wildcard syntax
import numpy as np                 # python numeric library
import pandas as pd                # create excel like data frames

# This is the test containing the weird Rs-factor hack.
data_Closed74RsDiv9 = pd.read_csv("homogeneous/closedsystem/blackoil/" + "masses_So72_HomogeneousClosedSystem_blackoil_divRsBy9.000000.dat" ,delim_whitespace=False)

# The normal simulations
data_Closed72 = pd.read_csv("homogeneous/closedsystem/blackoil/"+  "masses_So72_HomogeneousClosedSystem_blackoil.dat" ,delim_whitespace=False)
data_Closed74 = pd.read_csv("homogeneous/closedsystem/blackoil/"+  "masses_So74_HomogeneousClosedSystem_blackoil.dat" ,delim_whitespace=False)
data_TopOpen72 = pd.read_csv("homogeneous/topopensystem/blackoil/" + "masses_So72_HomogeneousTopOpenSystem_blackoil.dat",delim_whitespace=False)
data_TopOpen74 = pd.read_csv("homogeneous/topopensystem/blackoil/" + "masses_So74_HomogeneousTopOpenSystem_blackoil.dat",delim_whitespace=False)
data_BottomJect72 = pd.read_csv("homogeneous/bottominjection/blackoil/" + "masses_So72_HomogeneousBottomInjectionSystem_blackoil.dat",delim_whitespace=False)
data_BottomJect74 = pd.read_csv("homogeneous/bottominjection/blackoil/" + "masses_So74_HomogeneousBottomInjectionSystem_blackoil.dat",delim_whitespace=False)

tsz=17
fig = plt.figure(figsize=(20,7 ))

ax1=fig.add_axes([.1,.1, .8, .8])
ax1.grid(True)
ax1.tick_params(axis='both', which='major', labelsize=tsz)
plt.title(r'Time Evolution of Total Masses Inside the Modelling Domain; Homogeneous Domain Test.')
plt.xlabel('simulation time [s]', fontsize = tsz)
plt.ylabel('mass [kg/m]',fontsize = tsz)


#moG
ax1.plot(data_Closed72.time, data_Closed72.moG, '-', markersize=1,label="moG; Quasi Closed System; So_ini = 0.72")
#ax1.plot(data_Closed74.time, data_Closed74.moG, '-', markersize=1,label="moG; Quasi Closed System; So_ini = 0.74")

#mgG
ax1.plot(data_Closed72.time, data_Closed72.mgG, '-', markersize=1,label="mgG; Quasi Closed System; So_ini = 0.72")
#ax1.plot(data_Closed74.time, data_Closed74.mgG, '-', markersize=1,label="mgG; Quasi Closed System; So_ini = 0.74")
ax1.plot(data_TopOpen72.time, data_TopOpen72.mgG, '-', markersize=1,label="mgG; Top Open System; So_ini = 0.72")
#ax1.plot(data_TopOpen74.time, data_TopOpen74.mgG, '-', markersize=1,label="mgG; Top Open System; So_ini = 0.74")
ax1.plot(data_BottomJect72.time, data_BottomJect72.mgG, '-', markersize=1,label="mgG; Bottom Injection System; So_ini = 0.72")
#ax1.plot(data_BottomJect74.time, data_BottomJect74.mgG, '-', markersize=1,label="mgG; Bottom Injection System; So_ini = 0.74")

#mO
ax1.plot(data_Closed72.time, data_Closed72.mO, '-', markersize=1,label="mO; Quasi Closed System; So_ini = 0.72")
#ax1.plot(data_Closed74.time, data_Closed74.mO, '-', markersize=1,label="mO; Quasi Closed System; So_ini = 0.74")
ax1.plot(data_TopOpen72.time, data_TopOpen72.mO, '-', markersize=1,label="mO; Top Open System; So_ini = 0.72")
#ax1.plot(data_TopOpen74.time, data_TopOpen74.mO, '-', markersize=1,label="mO; Top Open System; So_ini = 0.74")
ax1.plot(data_BottomJect72.time, data_BottomJect72.mO, '-', markersize=1,label="mO; Bottom Injection System; So_ini = 0.72")
#ax1.plot(data_BottomJect74.time, data_BottomJect74.mO, '-', markersize=1,label="mO; Bottom Injection System; So_ini = 0.74")


# This plots the RsDivBy 9 simulation total masses
#ax1.plot(data_Closed72RsDiv9.time, data_Closed72RsDiv9.mgG, '-', markersize=1,label="mgG; Quasi Closed System; So_ini = 0.72; Rs divided by 9")


plt.legend(fontsize = tsz-6)
plt.tight_layout()
plt.show()
