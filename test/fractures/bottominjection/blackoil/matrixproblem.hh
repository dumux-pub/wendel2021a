// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The sub-problem for the matrix domain.
 */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_PROBLEM_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_PROBLEM_HH


#include <dune/common/indices.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/numeqvector.hh>

// include the base problem we inherit from
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

/*!
 * \brief The sub-problem for the matrix domain.
 */
template<class TypeTag>
class MatrixSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Scalar = typename GridVariables::Scalar;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    // some indices for convenience
    using Indices = BlackOilIndices;
    static constexpr int numComponents = 3;
    static constexpr int numPhases = 3;
    // copy some indices for convenience
    enum {
        pressureIdx = Indices::pressureIdx,
        oSaturationIdx = Indices::oSaturationIdx,
        wSaturationIdx = Indices::wSaturationIdx,

        // world dimension
        dimWorld = GridView::dimensionworld
    };
    static constexpr int wPhaseIdx = FluidSystem::wPhaseIdx;
    static constexpr int gPhaseIdx = FluidSystem::gPhaseIdx;
    static constexpr int oPhaseIdx = FluidSystem::oPhaseIdx;
    static constexpr int oCompIdx = Indices::conti0EqIdx + FluidSystem::oCompIdx; //!< Index of the mass conservation equation for the oil component
    static constexpr int wCompIdx = Indices::conti0EqIdx + FluidSystem::wCompIdx;//!< Index of the mass conservation equation for the water componen
    static constexpr int gCompIdx = Indices::conti0EqIdx + FluidSystem::gCompIdx;//!< Index of the mass conservation equation for the gas component
public:
    //! The constructor
    MatrixSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , time_(0.0)
    {
        //initializing the black oil fluidsystem
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        FluidSystem::initBegin();
         std::vector<std::pair<Scalar, Scalar> > Bg = {
            { 1.013529e+05, 9.998450e-01  },
            { 2757902.8,    0.03312601837829474 },
            { 5515805.6,    0.01656300918914737 },
            { 8273708.4,    0.011004575596857235 },
            { 11031611.2,   0.008253431697642926 },
            { 13789514.0,   0.006625203675658949 },
            { 16547416.8,   0.005502287798428617 },
            { 19305319.6,   0.004716246684367386 },
            { 22063222.4,   0.004154788745752222 },
            { 24821125.2,   0.0036494766009985734 },
            { 27579028.0,   0.0033126018378294743 },
            { 30336930.8,   0.003031872868521892 },
            { 33094833.6,   0.0027511438992143086 },
            { 35852736.4,   0.002526560723768243 },
            { 38610639.2,   0.002358123342183693 }
        };
        std::vector<std::pair<Scalar, Scalar> > Bo = {
            { 1.013529e+05, 1.000000e+00  },
            { 2757902.8,    1.012  },
            { 5515805.6,    1.0255 },
            { 8273708.4,    1.038  },
            { 11031611.2,   1.051  },
            { 13789514.0,   1.063  },
            { 16547416.8,   1.075  },
            { 19305319.6,   1.087  },
            { 22063222.4,   1.0985 },
            { 24821125.2,   1.11   },
            { 27579028.0,   1.12   },
            { 30336930.8,   1.13   },
            { 33094833.6,   1.14   },
            { 35852736.4,   1.148  },
            { 38610639.2,   1.155  }
        };
        // Division of all Rs spline values by a runtime factor WARNING: This is a hack and not very physical!!!
        rsDivisor_ = getParam<Scalar>("Problem.RsDivisor",1.0);
        std::vector<std::pair<Scalar, Scalar> > Rs = {
            { 1.013529e+05, 0.000000e+00 },
            { 2757902.8,    29.387772648867333/rsDivisor_ },
            { 5515805.6,    59.666083862851856/rsDivisor_ },
            { 8273708.4,    89.05385651171919 /rsDivisor_},
            { 11031611.2,   118.44162916058653/rsDivisor_ },
            { 13789514.0,   147.47318638340698/rsDivisor_ },
            { 16547416.8,   175.43609732808682/rsDivisor_ },
            { 19305319.6,   201.26171571648536/rsDivisor_ },
            { 22063222.4,   226.19679553976673/rsDivisor_ },
            { 24821125.2,   247.56972110257934/rsDivisor_ },
            { 27579028.0,   267.1615695351575 /rsDivisor_},
            { 30336930.8,   284.9723408375014 /rsDivisor_},
            { 33094833.6,   298.5085270272827 /rsDivisor_},
            { 35852736.4,   311.6884977910172 /rsDivisor_},
            { 38610639.2,   322.37496057242345/rsDivisor_ }
        };
        std::vector<std::pair<Scalar, Scalar> > muo = {
            { 1.013529e+05,  1.200000e-03 },
            { 2757902.8,     0.00117},
            { 5515805.6,     0.00114},
            { 8273708.4,     0.00111},
            { 11031611.2,    0.00108},
            { 13789514.0,    0.00106},
            { 16547416.8,    0.00103},
            { 19305319.6,    0.00100},
            { 22063222.4,    0.00098},
            { 24821125.2,    0.00095},
            { 27579028.0,    0.00094},
            { 30336930.8,    0.00092},
            { 33094833.6,    0.00091},
            { 35852736.4,    0.00090},
            { 38610639.2,    0.00089}
        };
        std::vector<std::pair<Scalar, Scalar> > mug = {
            { 1.013529e+05, 1.2500e-05   },
            { 2757902.8,    1.3000e-05   },
            { 5515805.6,    1.3500e-05   },
            { 8273708.4,    1.4000e-05   },
            { 11031611.2,   1.4500e-05   },
            { 13789514.0,   1.5000e-05   },
            { 16547416.8,   1.5500e-05   },
            { 19305319.6,   1.6000e-05   },
            { 22063222.4,   1.6500e-05   },
            { 24821125.2,   1.7000e-05   },
            { 27579028.0,   1.7500e-05   },
            { 30336930.8,   1.8000e-05   },
            { 33094833.6,   1.8500e-05   },
            { 35852736.4,   1.9000e-05   },
            { 38610639.2,   1.9500e-05   }
        };
        FluidSystem::setGasFormationVolumeFactor(Bg);
        FluidSystem::setOilFormationVolumeFactor(Bo);
        FluidSystem::setGasFormationFactor(Rs);
        FluidSystem::setOilViscosity(muo);
        FluidSystem::setGasViscosity(mug);
        FluidSystem::setWaterViscosity(9.6e-4);
        FluidSystem::setWaterCompressibility(1.450377e-10);
        FluidSystem::setSurfaceDensities(/*oil=*/720.51,
                                         /*water=*/1009.32,
                                         /*gas=*/1.1245);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++ phaseIdx)
            FluidSystem::setReferenceVolumeFactor(phaseIdx, 1.0);
        FluidSystem::initEnd();
        extrusionFactor_ = getParam<Scalar>("Problem.ExtrusionFactor");
        initialMatrixWaterSaturation_= getParam<Scalar>("InitialConditions.InitialMatrixWaterSaturation");
        initialMatrixOilSaturation_ = getParam<Scalar>("InitialConditions.InitialMatrixOilSaturation");
        initialOverburdenTopPressure_ = getParam<Scalar>("InitialConditions.InitialOverburdenTopPressure");
        // injection specifications
        injectionRate_ = getParam<Scalar>("Problem.InjectionRate");
        injectionPosition_ = getParam<Scalar>("Problem.InjectionPosition");
        injectionLength_ = getParam<Scalar>("Problem.InjectionLength");
        injectionDuration_ = getParam<Scalar>("Problem.InjectionDuration");
        time_ = 0.0;
        name_ = "Matrix";
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        values.setAllNeumann();
        const auto& yMax = this->gridGeometry().bBoxMax()[1]-eps_;
        if(globalPos[1] > yMax)
            values.setAllDirichlet(); // Plug the dirichlet fixpoint
        return values;
    }


    //! Specifies the type of interior boundary condition to be used on a sub-control volume face
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        // Here we set the type of condition to be used on faces that coincide
        // with a fracture. If Neumann is specified, a flux continuity condition
        // on the basis of the normal fracture permeability is evaluated.
        values.setAllNeumann();


        return values;
    }


    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        auto values = initialAtPos(globalPos);

        // use zero-saturation within the overburden
        if (globalPos[1] > 35.0)
        {
            values[oSaturationIdx] = 0.0;
            values[wSaturationIdx] = 1.0;
        }
        return values;
    }
    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */

    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        auto values = NumEqVector(0.0);
        // within the injection region
        if (globalPos[1] < 1e-6
            && globalPos[0] > injectionPosition_
            && globalPos[0] < injectionPosition_ + injectionLength_
            && time_ < injectionDuration_ + 1e-6)
        {
            values[wCompIdx] = injectionRate_;
        }
        return values;
    }
    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        // For the grid used here, the height of the domain is equal
        // to the maximum y-coordinate
        const auto domainHeight = this->gridGeometry().bBoxMax()[1];

        // we assume a constant water density of 1000 for initial conditions!
        const auto& g = this->spatialParams().gravity(globalPos);
        PrimaryVariables values;
        Scalar densityW = 1000.0;
        values[pressureIdx] = initialOverburdenTopPressure_ - (domainHeight - globalPos[1])*densityW*g[1];

        // in the overburden (and maybe in the upper meters of the shale there is no gas initially)
        if (globalPos[1] > 35.0)  // take 35 meters if you only want zero gas in the overburden
        {
            values[oSaturationIdx] = 0.0;
            values[wSaturationIdx] = 1.0;
        }
        // the shale formation contains gas initially
        else
        {
            values[wSaturationIdx] = initialMatrixWaterSaturation_;
            values[oSaturationIdx] = initialMatrixOilSaturation_;
        }
        return values;
    }

    //! computes the gas flux from the reservoir into the overburden
    template<class GridVariables, class SolutionVector, class Assembler, std::size_t id>
    Scalar computeGasFluxToOverburden(const GridVariables& gridVars,
                                      const SolutionVector& sol,
                                      const Assembler& assembler,
                                      const Dune::index_constant<id> domainId) const
    {
        Scalar flux = 0.0;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            // only proceed if elem could potentially be on the interface to overburden
            const auto geometry = element.geometry();
            if (geometry.center()[1] < 35.0 && geometry.center()[1] > 15.0)
            {
                couplingManagerPtr_->bindCouplingContext(domainId, element, assembler);

                auto fvGeometry = localView(this->gridGeometry());
                fvGeometry.bind(element);

                auto elemVolVars = localView(gridVars.curGridVolVars());
                elemVolVars.bind(element, fvGeometry, sol);

                auto elemFluxVarsCache = localView(gridVars.gridFluxVarsCache());
                elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

                for (const auto& scvf : scvfs(fvGeometry))
                {
                    if (scvf.boundary())
                        continue;

                    const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                    const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());

                    const bool isOnInterface = insideScv.center()[1] < 35.0 && outsideScv.center()[1] > 35.0;
                    if (isOnInterface)
                    {
                        using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
                        FluxVariables fluxVars;
                        fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

                        const auto gasCompIdx = gCompIdx;
                        for(int phIdx =0;phIdx<numPhases;phIdx++)
                        {
                            auto upwindTerm = [phIdx, gasCompIdx](const auto& volVars)
                            { return volVars.density(phIdx)*volVars.moleFraction(phIdx, gasCompIdx)*volVars.mobility(phIdx); };

                            flux += fluxVars.advectiveFlux(phIdx, upwindTerm);
                        }
                    }
                }
            }
        }

        return flux;
    }

    //! computes the influx of water into the domain
    //! This is for the output
    Scalar computeInjectionFlux() const
    {
        Scalar flux = 0.0;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bind(element);

            for (const auto& scvf : scvfs(fvGeometry))
                if (scvf.boundary())
                    flux -= neumannAtPos(scvf.ipGlobal())[wCompIdx]*scvf.area();
        }

        return flux;
    }

    //! computes the total masses
    template<class GridVariables, class SolutionVector, class Assembler, std::size_t id>
    std::vector<std::pair<std::string, Scalar>> getPostProcessingData(const GridVariables& gridVars,
                                const SolutionVector& x,
                                const Assembler& assembler,
                                const Dune::index_constant<id> domainId) const
    {
        using DataPair = std::pair<std::string, Scalar>;
        using Result = std::vector<DataPair>;
        Result masses;
        int i = 0;
        int idx_moG = i++;           masses.emplace_back(std::make_pair("Mat_moG",0.0));
        int idx_mgG = i++;           masses.emplace_back(std::make_pair("Mat_mgG",0.0));
        int idx_mG = i++;            masses.emplace_back(std::make_pair("Mat_mG",0.0));
        int idx_mO = i++;            masses.emplace_back(std::make_pair("Mat_mO",0.0));
        int idx_mW = i++;            masses.emplace_back(std::make_pair("Mat_mW",0.0));
        int idx_mTot = i++;          masses.emplace_back(std::make_pair("Mat_mTot",0.0));
        Scalar mass_oG = 0.0;
        Scalar mass_gG = 0.0;
        Scalar mass_G = 0.0;
        Scalar mass_O = 0.0;
        Scalar mass_W = 0.0;
        Scalar massTot = 0.0;

        for( const auto& element : elements(this->gridGeometry().gridView()) )
        {
            couplingManagerPtr_->bindCouplingContext(domainId, element, assembler);

            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                const Scalar poro = volVars.porosity();
                const Scalar So = volVars.saturation(oPhaseIdx);
                const Scalar Sg = volVars.saturation(gPhaseIdx);
                const Scalar XoG = volVars.massFraction(oPhaseIdx,gCompIdx);
                const Scalar XgG = volVars.massFraction(gPhaseIdx,gCompIdx);
                const Scalar rhog = volVars.density(gPhaseIdx);
                const Scalar rhoo = volVars.density(oPhaseIdx);
                const Scalar vol = elemVolVars[scv].extrusionFactor()*scv.volume();
                mass_oG += XoG*rhoo*So*vol*poro;
                mass_gG += XgG*rhog*Sg*vol*poro;
                for (int PhIdx=0; PhIdx < numPhases; PhIdx++)
                {
                    mass_G += volVars.saturation(PhIdx)*volVars.density(PhIdx)*volVars.massFraction(PhIdx, gCompIdx)*vol*poro;
                    mass_O += volVars.saturation(PhIdx)*volVars.density(PhIdx)*volVars.massFraction(PhIdx,oCompIdx )*vol*poro;
                    mass_W += volVars.saturation(PhIdx)*volVars.density(PhIdx)*volVars.massFraction(PhIdx, wCompIdx )*vol*poro;
                }
                massTot = mass_G+mass_O+mass_W;

            }
        }
        masses[idx_moG].second = mass_oG;
        masses[idx_mgG].second = mass_gG;
        masses[idx_mG].second = mass_G;
        masses[idx_mO].second = mass_O;
        masses[idx_mW].second = mass_W;
        masses[idx_mTot].second = massTot;

        return masses;
    }
    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! sets the current time
    void setTime(Scalar time)
    { time_ = time; }
    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Returns how much the domain is extruded at a given position.
     *
     * This means the factor by which a lower-dimensional
     * entity needs to be expanded to get a full-dimensional cell.
     */
    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    {
        return extrusionFactor_;
    }
private:

    std::shared_ptr<CouplingManager> couplingManagerPtr_;

    Scalar time_;
    Scalar initialShaleSaturation_;
    Scalar initialMatrixOilSaturation_;
    Scalar initialMatrixWaterSaturation_;
    Scalar initialOverburdenTopPressure_;
    Scalar extrusionFactor_;
    Scalar rsDivisor_;

    // injection specifics
    Scalar injectionRate_;
    Scalar injectionPosition_;
    Scalar injectionLength_;
    Scalar injectionDuration_;
    static constexpr Scalar eps_ = 1e-6;
    static constexpr Scalar yBd = 3.0;
    std::string name_;
};

} // end namespace Dumux

#endif
