// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 /*!
  * \file
  * \brief The properties file of the exercise on two-phase flow in fractured porous media.
  */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_PROPERTIES_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_PROPERTIES_HH


#include <myfluidsystem/blackoil/model.hh>
#include <myfluidsystem/blackoilfluidsystem.hh>

// Fracture sub-problem
// we use alu grid for the discretization of the fracture domain
// as this grid manager is able to represent network/surface grids
#include <dune/foamgrid/foamgrid.hh>
// we use a cell-centered finite volume scheme with tpfa here
#include <dumux/discretization/cctpfa.hh>
// the spatial parameters (permeabilities, material parameters etc.)
#include "fracturespatialparams.hh"
//The problem file, where setup-specific boundary and initial conditions are defined.
#include "fractureproblem.hh"


// Matrix sub-problem
// we use alu grid for the discretization of the matrix domain
#include <dune/alugrid/grid.hh>
// We are using the framework for models that consider coupling
// across the element facets of the bulk domain. This has some
// properties defined, which we have to inherit here. In this
// exercise we want to use a cell-centered finite volume scheme
// with tpfa.
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
// the spatial parameters (permeabilities, material parameters etc.)
#include "matrixspatialparams.hh"
//The problem file, where setup-specific boundary and initial conditions are defined.
#include "matrixproblem.hh"

namespace Dumux::Properties {

// create the type tag nodes
// Create new type tags
namespace TTag {

// create the typetag for fracture sub-problem
struct FractureProblemTypeTag { using InheritsFrom = std::tuple<BlackOil, CCTpfaModel>; };

// create the type tag node for the matrix sub-problem
// We need to put the facet-coupling type tag after the physics-related type tag
// because it overwrites some of the properties inherited from "BlackOil". This is
// necessary because we need e.g. a modified implementation of darcys law that
// evaluates the coupling conditions on faces that coincide with the fractures.
struct MatrixProblemTypeTag { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, BlackOil>; };
} // end namespace TTag

// Set the grid type for fracture sub-problem
template<class TypeTag>
struct Grid<TypeTag, TTag::FractureProblemTypeTag> { using type = Dune::FoamGrid<1, 2>; };

// Set the grid type for matrix sub-problem
template<class TypeTag>
struct Grid<TypeTag, TTag::MatrixProblemTypeTag> { using type = Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>; };

// Set the problem type for fracture sub-problem
template<class TypeTag>
struct Problem<TypeTag, TTag::FractureProblemTypeTag> { using type = FractureSubProblem<TypeTag>; };

// Set the grid type for matrix sub-problem
template<class TypeTag>
struct Problem<TypeTag, TTag::MatrixProblemTypeTag> { using type = MatrixSubProblem<TypeTag>; };

// Set the spatial parameters for fracture sub-problem
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::FractureProblemTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FractureSpatialParams<FVGridGeometry, Scalar>;
};

// Set the grid type for matrix sub-problem
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::MatrixProblemTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = MatrixSpatialParams<FVGridGeometry, Scalar>;
};

// the fluid system for fracture sub-problem
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::FractureProblemTypeTag>
{
 using type = FluidSystems::BlackOil<GetPropType<TypeTag, Properties::Scalar>>;
};

// Set the grid type for matrix sub-problem
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::MatrixProblemTypeTag>
{
 using type = FluidSystems::BlackOil<GetPropType<TypeTag, Properties::Scalar>>;
};

} // end namespace Dumux::Properties

#endif
