// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial parameters for the fracture sub-domain.
 */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_SPATIALPARAMS_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_SPATIALPARAMS_HH

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/3p/parkervangenuchten.hh>

namespace Dumux {

/*!
 * \brief The spatial params the two-phase facet coupling test
 */
template< class FVGridGeometry, class Scalar >
class MatrixSpatialParams
: public FVSpatialParams< FVGridGeometry, Scalar, MatrixSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = MatrixSpatialParams< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParams< FVGridGeometry, Scalar, ThisType >;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using GridView = typename FVGridGeometry::GridView;
    using Grid = typename GridView::Grid;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    // use a regularized van-genuchten material law
    using PcKrSw = FluidMatrix::ParkerVanGenuchten3PDefault<Scalar>;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! the constructor
    MatrixSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                        const std::string& paramGroup)
    : ParentType(fvGridGeometry)
    , pcKrSw_("SpatialParams")
    , pcKrSwOverburden_("SpatialParams.Overburden")
    {
        porosity_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity");
        permeability_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
        porosityOverburden_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Overburden.Porosity");
        permeabilityOverburden_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Overburden.Permeability");
        assignPermeabilities_();
        assignPorosities_();
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template<class ElementSolution>
    Scalar permeability(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    {
        const unsigned int elemIdx = this->gridGeometry().elementMapper().index(element);
        return permeabilities_[elemIdx];
    }

    //! Return the porosity
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                   const SubControlVolume& scv,
                   const ElementSolution& elemSol ) const
    {
        const unsigned int elemIdx = this->gridGeometry().elementMapper().index(element);
        return porosities_[elemIdx];
    }
    //! Return the material law parameters
    auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    { return globalPos[1] > 35.0 ? pcKrSwOverburden_ : pcKrSw_; }

private:
    void assignPermeabilities_()
    {
        permeabilities_.resize(this->gridGeometry().gridView().size(0), 0.0);
        for (const auto& element : elements(this->gridGeometry().gridView()  ) )
        {
            const auto& pos = element.geometry().center();
            const unsigned int elemIdx = this->gridGeometry().elementMapper().index(element);
            permeabilities_[elemIdx] = permeabilityAtPos_(pos);
        }
    }
    //! Return the porosity
    Scalar porosityAtPos_(const GlobalPosition& globalPos) const
    {
        if (globalPos[1] > 35.0)
            return porosityOverburden_;
        else if ((globalPos[1] > 32)||(globalPos[1] < 30))
            return porosity_;
        else return 0.05;
    }

    void assignPorosities_()
    {
        porosities_.resize(this->gridGeometry().gridView().size(0), 0.0);
        for (const auto& element : elements(this->gridGeometry().gridView()  ) )
        {
            const auto& pos = element.geometry().center();
            const unsigned int elemIdx = this->gridGeometry().elementMapper().index(element);
            porosities_[elemIdx] = porosityAtPos_(pos);
        }
    }

    PermeabilityType permeabilityAtPos_(const GlobalPosition& globalPos) const
    {
        const auto& yMax = this->gridGeometry().bBoxMax()[1]-eps_;
        if(globalPos[1] > yMax-yBd)
                return 1e-20; // Plug the dirichlet fixpoint
        if (globalPos[1] > 35.0)
            return permeabilityOverburden_;
        else if ((globalPos[1] > 32)||(globalPos[1] < 30))
            return permeability_;
        else return permeability_/10000.0;
    }

    static constexpr Scalar eps_ = 1e-6;
    static constexpr Scalar yBd = 3.0;
    Scalar porosity_;
    Scalar porosityOverburden_;
    PermeabilityType permeability_;
    PermeabilityType permeabilityOverburden_;
    mutable std::vector<Scalar> permeabilities_, porosities_;
    const PcKrSw pcKrSw_;
    const PcKrSw pcKrSwOverburden_;
};

} // end namespace Dumux

#endif
