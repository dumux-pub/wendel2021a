// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 /*!
  * \file
  * \brief The sub-problem for the fracture domain.
  */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_PROBLEM_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
// include the base problem we inherit from
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

/*!
  * \brief The sub-problem for the fracture domain in the exercise on two-phase flow in fractured porous media.
 */
template<class TypeTag>
class FractureSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using Scalar = typename GridVariables::Scalar;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using Indices = BlackOilIndices;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    static constexpr int numComponents = 3;
    static constexpr int numPhases = 3;
    // copy some indices for convenience
    enum {
        pressureIdx = Indices::pressureIdx,
        oSaturationIdx = Indices::oSaturationIdx,
        wSaturationIdx = Indices::wSaturationIdx,

        // world dimension
        dimWorld = GridView::dimensionworld
    };
    static constexpr int wPhaseIdx = FluidSystem::wPhaseIdx;
    static constexpr int gPhaseIdx = FluidSystem::gPhaseIdx;
    static constexpr int oPhaseIdx = FluidSystem::oPhaseIdx;
    static constexpr int oCompIdx = Indices::conti0EqIdx + FluidSystem::oCompIdx; //!< Index of the mass conservation equation for the oil component
    static constexpr int wCompIdx = Indices::conti0EqIdx + FluidSystem::wCompIdx;//!< Index of the mass conservation equation for the water componen
    static constexpr int gCompIdx = Indices::conti0EqIdx + FluidSystem::gCompIdx;//!< Index of the mass conservation equation for the gas component
public:
    //! The constructor
    FractureSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                       std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                       const std::string& paramGroup)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , time_(0.0)
    , aperture_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture"))
    , initialFracturesWaterSaturation_(getParamFromGroup<Scalar>(paramGroup, "InitialConditions.InitialFracturesWaterSaturation"))
    , initialFracturesOilSaturation_(getParamFromGroup<Scalar>(paramGroup, "InitialConditions.InitialFracturesOilSaturation"))
    , initialOverburdenTopPressure_(getParamFromGroup<Scalar>(paramGroup, "InitialConditions.InitialOverburdenTopPressure"))
    {
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        FluidSystem::initBegin();
         std::vector<std::pair<Scalar, Scalar> > Bg = {
            { 1.013529e+05, 9.998450e-01  },
            { 2757902.8,    0.03312601837829474 },
            { 5515805.6,    0.01656300918914737 },
            { 8273708.4,    0.011004575596857235 },
            { 11031611.2,   0.008253431697642926 },
            { 13789514.0,   0.006625203675658949 },
            { 16547416.8,   0.005502287798428617 },
            { 19305319.6,   0.004716246684367386 },
            { 22063222.4,   0.004154788745752222 },
            { 24821125.2,   0.0036494766009985734 },
            { 27579028.0,   0.0033126018378294743 },
            { 30336930.8,   0.003031872868521892 },
            { 33094833.6,   0.0027511438992143086 },
            { 35852736.4,   0.002526560723768243 },
            { 38610639.2,   0.002358123342183693 }
        };
        std::vector<std::pair<Scalar, Scalar> > Bo = {
            { 1.013529e+05, 1.000000e+00  },
            { 2757902.8,    1.012  },
            { 5515805.6,    1.0255 },
            { 8273708.4,    1.038  },
            { 11031611.2,   1.051  },
            { 13789514.0,   1.063  },
            { 16547416.8,   1.075  },
            { 19305319.6,   1.087  },
            { 22063222.4,   1.0985 },
            { 24821125.2,   1.11   },
            { 27579028.0,   1.12   },
            { 30336930.8,   1.13   },
            { 33094833.6,   1.14   },
            { 35852736.4,   1.148  },
            { 38610639.2,   1.155  }
        };
        // Division of all Rs spline values by a runtime factor WARNING: This is a hack and not very physical!!!
        rsDivisor_ = getParam<Scalar>("Problem.RsDivisor",1.0);
        std::vector<std::pair<Scalar, Scalar> > Rs = {
            { 1.013529e+05, 0.000000e+00 },
            { 2757902.8,    29.387772648867333/rsDivisor_ },
            { 5515805.6,    59.666083862851856/rsDivisor_ },
            { 8273708.4,    89.05385651171919 /rsDivisor_},
            { 11031611.2,   118.44162916058653/rsDivisor_ },
            { 13789514.0,   147.47318638340698/rsDivisor_ },
            { 16547416.8,   175.43609732808682/rsDivisor_ },
            { 19305319.6,   201.26171571648536/rsDivisor_ },
            { 22063222.4,   226.19679553976673/rsDivisor_ },
            { 24821125.2,   247.56972110257934/rsDivisor_ },
            { 27579028.0,   267.1615695351575 /rsDivisor_},
            { 30336930.8,   284.9723408375014 /rsDivisor_},
            { 33094833.6,   298.5085270272827 /rsDivisor_},
            { 35852736.4,   311.6884977910172 /rsDivisor_},
            { 38610639.2,   322.37496057242345/rsDivisor_ }
        };
        std::vector<std::pair<Scalar, Scalar> > muo = {
            { 1.013529e+05,  1.200000e-03 },
            { 2757902.8,     0.00117},
            { 5515805.6,     0.00114},
            { 8273708.4,     0.00111},
            { 11031611.2,    0.00108},
            { 13789514.0,    0.00106},
            { 16547416.8,    0.00103},
            { 19305319.6,    0.00100},
            { 22063222.4,    0.00098},
            { 24821125.2,    0.00095},
            { 27579028.0,    0.00094},
            { 30336930.8,    0.00092},
            { 33094833.6,    0.00091},
            { 35852736.4,    0.00090},
            { 38610639.2,    0.00089}
        };
        std::vector<std::pair<Scalar, Scalar> > mug = {
            { 1.013529e+05, 1.2500e-05   },
            { 2757902.8,    1.3000e-05   },
            { 5515805.6,    1.3500e-05   },
            { 8273708.4,    1.4000e-05   },
            { 11031611.2,   1.4500e-05   },
            { 13789514.0,   1.5000e-05   },
            { 16547416.8,   1.5500e-05   },
            { 19305319.6,   1.6000e-05   },
            { 22063222.4,   1.6500e-05   },
            { 24821125.2,   1.7000e-05   },
            { 27579028.0,   1.7500e-05   },
            { 30336930.8,   1.8000e-05   },
            { 33094833.6,   1.8500e-05   },
            { 35852736.4,   1.9000e-05   },
            { 38610639.2,   1.9500e-05   }
        };
        FluidSystem::setGasFormationVolumeFactor(Bg);
        FluidSystem::setOilFormationVolumeFactor(Bo);
        FluidSystem::setGasFormationFactor(Rs);
        FluidSystem::setOilViscosity(muo);
        FluidSystem::setGasViscosity(mug);
        FluidSystem::setWaterViscosity(9.6e-4);
        FluidSystem::setWaterCompressibility(1.450377e-10);
        FluidSystem::setSurfaceDensities(/*oil=*/720.51,
                                         /*water=*/1009.32,
                                         /*gas=*/1.1245);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++ phaseIdx)
            FluidSystem::setReferenceVolumeFactor(phaseIdx, 1.0);
        FluidSystem::initEnd();
        extrusionFactor_ = getParam<Scalar>("Problem.ExtrusionFactor");
        name_ = "Fractures";
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        // We only use no-flow boundary conditions for all immersed fractures
        // in the domain (fracture tips that do not touch the domain boundary)
        // Otherwise, we would lose mass leaving across the fracture tips.
        values.setAllNeumann();

        return values;
    }

    //! Evaluate the source term in a sub-control volume of an element
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain using the function in the coupling manager
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);

        // these sources are in kg/s, divide by volume and extrusion to have it in kg/s/m³
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

    //! Set the aperture as extrusion factor.
    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    {
        // We treat the fractures as lower-dimensional in the grid,
        // but we have to give it the aperture as extrusion factor
        // such that the dimensions are correct in the end.
        return aperture_;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return initialAtPos(globalPos); }

    //! evaluates the Neumann boundary condition for a given position
    NumEqVector neumannAtPos(const GlobalPosition& globalPos) const
    {
        auto values = NumEqVector(0.0);

        return values;
    }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        // For the grid used here, the height of the domain is 52.5 m
        const auto domainHeight = 52.5;

        const auto& g = this->spatialParams().gravity(globalPos);
        PrimaryVariables values;
        Scalar densityW = 1000.0;
        values[pressureIdx] = initialOverburdenTopPressure_ - (domainHeight - globalPos[1])*densityW*g[1];
        values[oSaturationIdx] = initialFracturesOilSaturation_;
        values[wSaturationIdx] = initialFracturesWaterSaturation_;
        return values;
    }

    //! computes the influx of water into the domain
    Scalar computeInjectionFlux() const
    {
        Scalar flux = 0.0;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bind(element);

            for (const auto& scvf : scvfs(fvGeometry))
                if (scvf.boundary())
                    flux -= neumannAtPos(scvf.ipGlobal())[wCompIdx]
                            *scvf.area()
                            *extrusionFactorAtPos(scvf.ipGlobal());
        }

        return flux;
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! sets the current time
    void setTime(Scalar time)
    { time_ = time; }

    //! computes the total masses of gas in oil and gas in gas.
    template<class GridVariables, class SolutionVector, class Assembler, std::size_t id>
    std::vector<std::pair<std::string, Scalar>> getPostProcessingData(const GridVariables& gridVars,
                                const SolutionVector& x,
                                const Assembler& assembler,
                                const Dune::index_constant<id> domainId) const
    {
        using DataPair = std::pair<std::string, Scalar>;
        using Result = std::vector<DataPair>;
        Result masses;
        int i = 0;
        int idx_moG = i++;           masses.emplace_back(std::make_pair("Frc_moG",0.0));
        int idx_mgG = i++;           masses.emplace_back(std::make_pair("Frc_mgG",0.0));
        int idx_mG = i++;            masses.emplace_back(std::make_pair("Frc_mG",0.0));
        int idx_mO = i++;            masses.emplace_back(std::make_pair("Frc_mO",0.0));
        int idx_mW = i++;            masses.emplace_back(std::make_pair("Frc_mW",0.0));
        int idx_mTot = i++;          masses.emplace_back(std::make_pair("Frc_mTot",0.0));
        Scalar mass_oG = 0.0;
        Scalar mass_gG = 0.0;
        Scalar mass_G = 0.0;
        Scalar mass_O = 0.0;
        Scalar mass_W = 0.0;
        Scalar massTot = 0.0;

        for( const auto& element : elements(this->gridGeometry().gridView()) )
        {
            couplingManagerPtr_->bindCouplingContext(domainId, element, assembler);

            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                const Scalar poro = volVars.porosity();
                const Scalar So = volVars.saturation(oPhaseIdx);
                const Scalar Sg = volVars.saturation(gPhaseIdx);
                const Scalar XoG = volVars.massFraction(oPhaseIdx,gCompIdx);
                const Scalar XgG = volVars.massFraction(gPhaseIdx,gCompIdx);
                const Scalar rhog = volVars.density(gPhaseIdx);
                const Scalar rhoo = volVars.density(oPhaseIdx);
                const Scalar vol = elemVolVars[scv].extrusionFactor()*scv.volume();
                // Get the part that is for m_oG
                // XoG*r_o*S_o
                mass_oG += XoG*rhoo*So*vol*poro;
                mass_gG += XgG*rhog*Sg*vol*poro;
                for (int PhIdx=0; PhIdx < numPhases; PhIdx++)
                {
                    mass_G += volVars.saturation(PhIdx)*volVars.density(PhIdx)*volVars.massFraction(PhIdx, gCompIdx)*vol*poro;
                    mass_O += volVars.saturation(PhIdx)*volVars.density(PhIdx)*volVars.massFraction(PhIdx,oCompIdx )*vol*poro;
                    mass_W += volVars.saturation(PhIdx)*volVars.density(PhIdx)*volVars.massFraction(PhIdx, wCompIdx )*vol*poro;
                }
                massTot = mass_G+mass_O+mass_W;
            }
        }
        masses[idx_moG].second = mass_oG;
        masses[idx_mgG].second = mass_gG;
        masses[idx_mG].second = mass_G;
        masses[idx_mO].second = mass_O;
        masses[idx_mW].second = mass_W;
        masses[idx_mTot].second = massTot;

        return masses;
    }
    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Returns how much the domain is extruded at a given position.
     *
     * TODO: A better comment.
     * We assume that the fractures are therewith extendend into the "screen" by the same length as the matrix.
     */
    Scalar domainExtrusionFactorAtPos(const GlobalPosition &globalPos) const
    {
        return extrusionFactor_;
    }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;

    Scalar time_;
    Scalar aperture_;
    Scalar extrusionFactor_;
    Scalar initialFracturesWaterSaturation_;
    Scalar initialFracturesOilSaturation_;
    Scalar initialOverburdenTopPressure_;

    Scalar rsDivisor_;
    static constexpr Scalar yBd = 3.0;
    static constexpr Scalar xBd = 3.0;
    static constexpr Scalar eps_ = 1e-6;
    std::string name_;
};

} // end namespace Dumux

#endif
