// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for the exercise on two-phase flow in fractured porous media.
 */
#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

// include the properties header
#include "properties.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/assembly/diffmethod.hh>

#include <dumux/linear/seqsolverbackend.hh>
#if CHECKRELATIVESHIFTREDUCTION==0
#include <dumux/multidomain/newtonsolver.hh>
#elif CHECKRELATIVESHIFTREDUCTION==1
#include <mynewtonsolver/newtonsolverwithreductioncheking.hh>
#endif
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>

#include <dumux/io/vtkoutputmodule.hh>

namespace Dumux {
// Define some types for this test so that we can set them as properties below and
// reuse them again in the main function with only one single definition of them here
using MatrixTypeTag = Properties::TTag::MatrixProblemTypeTag;
using FractureTypeTag = Properties::TTag::FractureProblemTypeTag;
using MatrixFVGridGeometry = GetPropType<MatrixTypeTag, Properties::GridGeometry>;
using FractureFVGridGeometry = GetPropType<FractureTypeTag, Properties::GridGeometry>;
using TheMultiDomainTraits = Dumux::MultiDomainTraits<MatrixTypeTag, FractureTypeTag>;
using TheCouplingMapper = Dumux::FacetCouplingMapper<MatrixFVGridGeometry, FractureFVGridGeometry>;
using TheCouplingManager = Dumux::FacetCouplingManager<TheMultiDomainTraits, TheCouplingMapper>;

// set the coupling manager property in the sub-problems
namespace Properties {
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::MatrixProblemTypeTag> { using type = TheCouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FractureProblemTypeTag> { using type = TheCouplingManager; };
} // end namespace Properties
} // end namespace Dumux

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // instantiate the grids with the grid manager
    using MatrixGrid = GetPropType<MatrixTypeTag, Properties::Grid>;
    using FractureGrid = GetPropType<FractureTypeTag, Properties::Grid>;
    using GridManager = Dumux::FacetCouplingGridManager<MatrixGrid, FractureGrid>;
    GridManager gridManager;
    gridManager.init();
    gridManager.loadBalance();

    // we compute on the leaf grid views (get them from grid manager)
    // the grid ids correspond to the order of the grids passed to the manager (see above)
    static constexpr std::size_t matrixGridId = 0;
    static constexpr std::size_t fractureGridId = 1;
    const auto& matrixGridView = gridManager.template grid<matrixGridId>().leafGridView();
    const auto& fractureGridView = gridManager.template grid<fractureGridId>().leafGridView();

    // create the finite volume grid geometries
    auto matrixFvGridGeometry = std::make_shared<MatrixFVGridGeometry>(matrixGridView);
    auto fractureFvGridGeometry = std::make_shared<FractureFVGridGeometry>(fractureGridView);

    // the problems (boundary/initial conditions etc)
    using MatrixProblem = GetPropType<MatrixTypeTag, Properties::Problem>;
    using FractureProblem = GetPropType<FractureTypeTag, Properties::Problem>;

    // make the spatial parameters and problems
    auto matrixSpatialParams = std::make_shared<typename MatrixProblem::SpatialParams>(matrixFvGridGeometry, "Matrix");
    auto matrixProblem = std::make_shared<MatrixProblem>(matrixFvGridGeometry, matrixSpatialParams, "Matrix");

    auto fractureSpatialParams = std::make_shared<typename FractureProblem::SpatialParams>(fractureFvGridGeometry, "Fracture");
    auto fractureProblem = std::make_shared<FractureProblem>(fractureFvGridGeometry, fractureSpatialParams, "Fracture");

    // the solution vector
    using SolutionVector = typename TheMultiDomainTraits::SolutionVector;
    SolutionVector x, xOld;

    // The domain ids within the multi-domain framework.
    // They do not necessarily have to be the same as the grid ids
    // in case you have more subdomains involved. We domain ids
    // correspond to the order of the type tags passed to the multidomain
    // traits (see definition of the traits class at the beginning of this file)
    static const auto matrixDomainId = typename TheMultiDomainTraits::template SubDomain<0>::Index();
    static const auto fractureDomainId = typename TheMultiDomainTraits::template SubDomain<1>::Index();

    // resize the solution vector and write initial solution to it
    x[matrixDomainId].resize(matrixFvGridGeometry->numDofs());
    x[fractureDomainId].resize(fractureFvGridGeometry->numDofs());
    matrixProblem->applyInitialSolution(x[matrixDomainId]);
    fractureProblem->applyInitialSolution(x[fractureDomainId]);

    // instantiate the class holding the coupling maps between the domains
    // this needs the information on embeddings (connectivity between matrix
    // and fracture domain). This information is extracted directly from the
    // grid during file read and can therefore be obtained from the grid manager.
    const auto embeddings = gridManager.getEmbeddings();
    auto couplingMapper = std::make_shared<TheCouplingMapper>();
    couplingMapper->update(*matrixFvGridGeometry, *fractureFvGridGeometry, embeddings);

    // the coupling manager (needs the coupling mapper)
    auto couplingManager = std::make_shared<TheCouplingManager>();
    couplingManager->init(matrixProblem, fractureProblem, couplingMapper, x);

    // we have to set coupling manager pointer in sub-problems
    // they also have to be made accessible in them (see e.g. matrixproblem.hh)
    matrixProblem->setCouplingManager(couplingManager);
    fractureProblem->setCouplingManager(couplingManager);

    // the grid variables
    using MatrixGridVariables = GetPropType<MatrixTypeTag, Properties::GridVariables>;
    using FractureGridVariables = GetPropType<FractureTypeTag, Properties::GridVariables>;
    auto matrixGridVariables = std::make_shared<MatrixGridVariables>(matrixProblem, matrixFvGridGeometry);
    auto fractureGridVariables = std::make_shared<FractureGridVariables>(fractureProblem, fractureFvGridGeometry);
    xOld = x;
    matrixGridVariables->init(x[matrixDomainId]);
    fractureGridVariables->init(x[fractureDomainId]);

    // intialize the vtk output modules
    bool enableVtkOutput = getParam<bool>("Problem.EnableVtkOutput",true);
    using MatrixOutputModule = VtkOutputModule<MatrixGridVariables, GetPropType<MatrixTypeTag, Properties::SolutionVector>>;
    using FractureOutputModule = VtkOutputModule<FractureGridVariables, GetPropType<FractureTypeTag, Properties::SolutionVector>>;

        MatrixOutputModule matrixVtkWriter(*matrixGridVariables, x[matrixDomainId], matrixProblem->name(), "Matrix");
        FractureOutputModule fractureVtkWriter(*fractureGridVariables, x[fractureDomainId], fractureProblem->name(), "Fracture");

    // Add model specific output fields
    using MatrixIOFields = GetPropType<MatrixTypeTag, Properties::IOFields>;
    using FractureIOFields = GetPropType<FractureTypeTag, Properties::IOFields>;
    MatrixIOFields::initOutputModule(matrixVtkWriter);
    FractureIOFields::initOutputModule(fractureVtkWriter);

    // write out initial solution
    if(enableVtkOutput)
    {
        matrixVtkWriter.write(0.0);
        fractureVtkWriter.write(0.0);
    }
    // get some time loop parameters
    const auto tEnd = getParam<double>("TimeLoop.TEnd");
    const auto maxDt = getParam<double>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<double>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoop = std::make_shared< CheckPointTimeLoop<double> >(/*startTime*/0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler for the coupled problem
    using Assembler = MultiDomainFVAssembler<TheMultiDomainTraits, TheCouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(matrixProblem, fractureProblem),
                                                  std::make_tuple(matrixFvGridGeometry, fractureFvGridGeometry),
                                                  std::make_tuple(matrixGridVariables, fractureGridVariables),
                                                  couplingManager,
                                                  timeLoop,
                                                  xOld );

    // the linear solver
    using LinearSolver = UMFPackBackend;//ILU0BiCGSTABBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, TheCouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
    static unsigned int outputPrecision = 12;
    std::ofstream outputfile, outputfile2, massesOutputFile;
    if(getParam<double>("Problem.RsDivisor",1.0) != 1.0)
    {
        outputfile.open("escapedGas_divRsBy" +  std::to_string(getParam<double>("Problem.RsDivisor",1.0))+ ".dat", std::ios::out);
        outputfile2.open("injectedWater_divRsBy" +  std::to_string(getParam<double>("Problem.RsDivisor",1.0))+ ".dat", std::ios::out);
        massesOutputFile.open(getParam<std::string>("Problem.OutputfileMasses") +  std::to_string(getParam<double>("Problem.RsDivisor",1.0))+ ".dat", std::ios::out);
    }
    else
    {
        outputfile.open("escapedGas.dat", std::ios::out);
        outputfile2.open("injectedWater.dat", std::ios::out);
        massesOutputFile.open(getParam<std::string>("Problem.OutputfileMasses") +  ".dat", std::ios::out);
    }

    outputfile << std::scientific << std::setprecision(outputPrecision);
    outputfile2 << std::scientific << std::setprecision(outputPrecision);
    outputfile << "time" << "\t" << "EscapeGas [kg]" << std::endl;
    outputfile2 << "time" << "\t" << "InjectedWater [kg]" << std::endl;
    massesOutputFile << std::scientific << std::setprecision(outputPrecision);
    // We want to get the initial values into the simulation. Checking for system closedness
    // Get postprocessing information
    const auto matrixData = matrixProblem->getPostProcessingData(*matrixGridVariables, x[matrixDomainId],*assembler, matrixDomainId);
    const auto fracturesData = fractureProblem->getPostProcessingData(*fractureGridVariables, x[fractureDomainId], *assembler, fractureDomainId);
    // Headline in csv file
    double injectedWater = 0.0;
    double escapedGas = 0.0;
    massesOutputFile << "time,";
    for(int ii =0;ii<matrixData.size();++ii)
    {
        massesOutputFile<< matrixData[ii].first <<",";
    }
    // fractures data comes next
    for(int ii =0; ii<fracturesData.size()-1; ++ii)
    {
        massesOutputFile << fracturesData[ii].first << ",";
    }
    massesOutputFile << fracturesData.back().first << ",mGTot,mOTot,mWTot,diffmGTot,diffmOTot,diffmWTot,diffmTot,mTot" <<std::endl;// Finishing a row!
    // write data into file
    massesOutputFile << timeLoop->time() <<",";
    for (int ii=0; ii<matrixData.size();++ii)
    {
        massesOutputFile << matrixData[ii].second << ",";
    }
    // fractures data comes next
    for(int ii =0; ii<fracturesData.size(); ++ii)
    {
        massesOutputFile << fracturesData[ii].second <<",";
    }
    // Lambda for data extraction.
    const auto getData = [] (const auto& data, const std::string& key)
    {
        auto it = std::find_if(data.begin(), data.end(),
                               [&key] (const auto& pair)
                               { return pair.first == key; });
        if (it == data.end())
            DUNE_THROW(Dune::InvalidStateException, "Could not find data");

        return it->second;
    };
    // Summing the contributions for balance
    // Total System Contribution
    const auto mGfrac = getData(fracturesData, "Frc_mG");
    const auto mOfrac = getData(fracturesData, "Frc_mO");
    const auto mWfrac = getData(fracturesData, "Frc_mW");
    const auto mGmat = getData(matrixData, "Mat_mG");
    const auto mOmat = getData(matrixData, "Mat_mO");
    const auto mWmat = getData(matrixData, "Mat_mW");
    // get Total inside Component related masses
    const auto  mGTot = mGfrac+mGmat;
    const auto  mOTot = mOfrac+mOmat;
    const auto  mWTot = mWfrac+mWmat;
    // get totals
    const auto mTot = mGTot+mOTot+mWTot;
    // Write out the totals of component related masses for the complete system
    massesOutputFile<< mGTot<<",";
    massesOutputFile<< mOTot<<",";
    massesOutputFile<< mWTot<<",";
    // At the end, the total system mass is written into the output file
    massesOutputFile << mTot<< std::endl;
    // Statistical parameters to check the "closedness" of the system
    // They are initialized here.
    double summationsMasses=mTot; // This is x_0 for the arithmetic mean estimation!
    std::vector<double> massAtTimeStep;
    massAtTimeStep.emplace_back(mTot);
    double arithmeticMean;
    double summationsVariances, stdDeviation;
    // time loop
    timeLoop->start(); do
    {
        // tell the problems the time for which it is solved
        matrixProblem->setTime(timeLoop->time()+timeLoop->timeStepSize());
        fractureProblem->setTime(timeLoop->time()+timeLoop->timeStepSize());

        // solve the non-linear system with time step control
        newtonSolver->solve(x, *timeLoop);

        // print to the terminal the amount of gas escaped from the reservoir up to now
        std::cout << std::endl;
        const auto injectedInMatrix = matrixProblem->computeInjectionFlux()*timeLoop->timeStepSize();
        const auto injectedInFracture = fractureProblem->computeInjectionFlux()*timeLoop->timeStepSize();
        injectedWater += injectedInMatrix;
        injectedWater += injectedInFracture;
        std::cout << "Injected water in matrix in this time step: " << injectedInMatrix << " kg" <<std::endl;
        std::cout << "Injected water in fractures in this time step: " << injectedInFracture << " kg" << std::endl;
        std::cout << "Injected water so far: " << injectedWater << " kg" << std::endl;

        std::cout << std::endl;
        const auto escapedFromMatrix = timeLoop->timeStepSize() *matrixProblem->computeGasFluxToOverburden(*matrixGridVariables,
                                                                                                           x[matrixDomainId],
                                                                                                           *assembler,
                                                                                                           matrixDomainId);
        escapedGas += escapedFromMatrix;
        std::cout << "Escaped gas in this time step: " << escapedFromMatrix << " kg" << std::endl;
        std::cout << "Escaped gas so far: " << escapedGas << " kg" << std::endl << std::endl;
        outputfile << timeLoop->time() << "\t" << escapedGas << std::endl;
        outputfile2 << timeLoop->time() << "\t" << injectedWater << std::endl;

        // make the new solution the old solution
        xOld = x;
        matrixGridVariables->advanceTimeStep();
        fractureGridVariables->advanceTimeStep();


        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        if (!hasParam("TimeLoop.EpisodeLength") || timeLoop->isCheckPoint() || timeLoop->finished() || timeLoop->timeStepIndex() == 1)
        {
            if(enableVtkOutput)
            {
                matrixVtkWriter.write(timeLoop->time());
                fractureVtkWriter.write(timeLoop->time());
            }
        }

        // Get postprocessing information
        const auto matrixData = matrixProblem->getPostProcessingData(*matrixGridVariables, x[matrixDomainId],*assembler, matrixDomainId);
        const auto fracturesData = fractureProblem->getPostProcessingData(*fractureGridVariables, x[fractureDomainId], *assembler, fractureDomainId);
        // write data into file
        massesOutputFile << timeLoop->time() <<",";
        for (int ii=0; ii<matrixData.size();++ii)
        {
            massesOutputFile << matrixData[ii].second << ",";
        }
        // fractures data comes next
        for(int ii =0; ii<fracturesData.size(); ++ii)
        {
            massesOutputFile << fracturesData[ii].second <<",";
        }
        // Lambda for data extraction.
        const auto getData = [] (const auto& data, const std::string& key)
        {
            auto it = std::find_if(data.begin(), data.end(),
                                   [&key] (const auto& pair)
                                   { return pair.first == key; });
            if (it == data.end())
                DUNE_THROW(Dune::InvalidStateException, "Could not find data");

            return it->second;
        };
        // Summing the contributions for balance
        // Total System Contribution
        const auto mGfrac = getData(fracturesData, "Frc_mG");
        const auto mOfrac = getData(fracturesData, "Frc_mO");
        const auto mWfrac = getData(fracturesData, "Frc_mW");
        const auto mGmat = getData(matrixData, "Mat_mG");
        const auto mOmat = getData(matrixData, "Mat_mO");
        const auto mWmat = getData(matrixData, "Mat_mW");
        // get Total inside Component related masses
        const auto  mGTot = mGfrac+mGmat;
        const auto  mOTot = mOfrac+mOmat;
        const auto  mWTot = mWfrac+mWmat;
        // get totals
        const auto mTot = mGTot+mOTot+mWTot;
        // Write out the totals of component related masses for the complete system
        massesOutputFile<< mGTot<<",";
        massesOutputFile<< mOTot<<",";
        massesOutputFile<< mWTot<<",";
        // At the end, the total system mass is written into the output file
        massesOutputFile << mTot<< std::endl;

        // Statistics for checking system closedness
        massAtTimeStep.emplace_back(mTot );
        summationsMasses += mTot;
        arithmeticMean = summationsMasses/((double)timeLoop->timeStepIndex() +1); // first is initial conditions, so we have numTimeSteps + 1 data points!
        summationsVariances=0.0;
        for (int i =0; i<massAtTimeStep.size();i++)
        {
            summationsVariances += (massAtTimeStep[i] - arithmeticMean)*(massAtTimeStep[i] - arithmeticMean);
        }
        stdDeviation = std::sqrt(summationsVariances)/((double)timeLoop->timeStepIndex());
        std::cout << std::setprecision(18) << "Total Mass arithmetic mean = " << arithmeticMean << " Standard deviation = " << stdDeviation << std::endl;
        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the Newton solver
        timeLoop->setTimeStepSize(newtonSolver->suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());
    outputfile.close();
    outputfile2.close();
    massesOutputFile.close();

    // output some Newton statistics
    newtonSolver->report();

    // report time loop statistics
    timeLoop->finalize();

    // print dumux message to say goodbye
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;

}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
