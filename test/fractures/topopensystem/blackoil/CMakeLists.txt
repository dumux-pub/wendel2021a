dune_symlink_to_source_files(FILES "grids" "fracture_blackoil.input" "plot.p")

# test for the exercise
dumux_add_test(NAME fracture_blackoil_topopensystem
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )"
              COMPILE_DEFINITIONS CHECKRELATIVESHIFTREDUCTION=0
              SOURCES fractures.cc)
dumux_add_test(NAME fracture_blackoil_topopensystem_checkrelshiftreduction
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )"
              COMPILE_DEFINITIONS CHECKRELATIVESHIFTREDUCTION=1
              SOURCES fractures.cc)
