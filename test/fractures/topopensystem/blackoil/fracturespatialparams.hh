// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial parameters for the fracture sub-domain.
 */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_SPATIALPARAMS_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_SPATIALPARAMS_HH

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/3p/parkervangenuchten.hh>

namespace Dumux {

/*!
 * \brief The spatial params the two-phase facet coupling test
 */
template< class FVGridGeometry, class Scalar >
class FractureSpatialParams
: public FVSpatialParams< FVGridGeometry, Scalar, FractureSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = FractureSpatialParams< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParams< FVGridGeometry, Scalar, ThisType >;

    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using GridView = typename FVGridGeometry::GridView;
    using Grid = typename GridView::Grid;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    // use a regularized van-genuchten material law
    using PcKrSw = FluidMatrix::ParkerVanGenuchten3PDefault<Scalar>;

    // we identify those fractures as barriers, that have a domain marker
    // of 2. This is what is set in the grid file (see grids/complex.geo)
    static constexpr int barriersDomainMarker = 2;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! the constructor
    FractureSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                          const std::string& paramGroup)
    : ParentType(fvGridGeometry)
    , pcKrSwFracture_("SpatialParams.Fracture")
    {
        porosity_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Fracture.Porosity");
        permeability_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Fracture.Permeability");
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template< class ElementSolution >
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    { return permeability_; }

    //! Return the porosity
    template< class ElementSolution >
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    { return porosity_; }

    //! Return the material law parameters
    auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    { return pcKrSwFracture_; }


private:
    Scalar porosity_;
    PermeabilityType permeability_;
    const PcKrSw pcKrSwFracture_;
};

} // end namespace Dumux

#endif
