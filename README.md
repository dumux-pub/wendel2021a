# wendel2021a

Welcome to the dumux pub module for wendel2021a
======================================================

This module contains the source code for the tests in the master thesis

__K. Wendel (2021)__, Implementing and Testing a Standard Black Oil Model in Dumux


Installation
============

For building from source create a folder and download [installwendel2021a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/wendel2021a/-/blob/master/installwendel\021a.sh) from this repository

to that folder and run the script with

```
chmod u+x installwendel2021a.sh
./installwendel2021a.sh
```

If you don't have UMFPackBackend installed, it can be installed with
```
sudo apt install libsuitesparse-dev
```

After the script has run successfully, you may build all executables

```
cd wendel2021a/build-cmake/test/
make bottominjection_box
make bottominjection_tpfa
make bottominjection_box_checkrelshiftreduction
make bottominjection_tpfa_checkrelshiftreduction
make closedsystem_test_blackoil_box
make closedsystem_test_blackoil_box_checkrelshiftreduction
make closedsystem_test_blackoil_tpfa
make closedsystem_test_blackoil_tpfa_checkrelshiftreduction
make topopensystem_test_blackoil_box
make topopensystem_test_blackoil_box_checkrelshiftreduction
make topopensystem_test_blackoil_tpfa
make topopensystem_test_blackoil_tpfa_checkrelshiftreduction
make fracture_blackoil_bottominjection
make fracture_blackoil_bottominjection_checkrelshiftreduction
make fracture_blackoil_closedsystem
make fracture_blackoil_closedsystem_checkrelshiftreduction
make fracture_blackoil_topopensystem
make fracture_blackoil_topopensystem_checkrelshiftreduction
```
and you can run them individually. They are located in the build-cmake folder according to the following paths:
```
test/homogeneous/bottominjection/blackoil/bottominjection_box
test/homogeneous/bottominjection/blackoil/bottominjection_tpfa
test/homogeneous/bottominjection/blackoil/bottominjection_box_checkrelshiftreduction
test/homogeneous/bottominjection/blackoil/bottominjection_tpfa_checkrelshiftreduction
test/homogeneous/closedsystem/blackoil/closedsystem_test_blackoil_box
test/homogeneous/closedsystem/blackoil/closedsystem_test_blackoil_box_checkrelshiftreduction
test/homogeneous/closedsystem/blackoil/closedsystem_test_blackoil_tpfa
test/homogeneous/closedsystem/blackoil/closedsystem_test_blackoil_tpfa_checkrelshiftreduction
test/homogeneous/topopensystem/blackoil/topopensystem_test_blackoil_box
test/homogeneous/topopensystem/blackoil/topopensystem_test_blackoil_box_checkrelshiftreduction
test/homogeneous/topopensystem/blackoil/topopensystem_test_blackoil_tpfa
test/homogeneous/topopensystem/blackoil/topopensystem_test_blackoil_tpfa_checkrelshiftreduction
test/fractures/bottominjection/blackoil/fracture_blackoil_bottominjection
test/fractures/bottominjection/blackoil/fracture_blackoil_bottominjection_checkrelshiftreduction
test/fractures/closedsystem/blackoil/fracture_blackoil_closedsystem_checkrelshiftreduction
test/fractures/closedsystem/blackoil/fracture_blackoil_closedsystem
test/fractures/topopensystem/blackoil/fracture_blackoil_topopensystem
test/fractures/topopensystem/blackoil/fracture_blackoil_topopensystem_checkrelshiftreduction
```

The tests with postfix "checkrelshiftreduction" use a newtonsolver where the relative shift is checked for reduction every iteration. The time step is reduced if reduction is very low after the 2nd iteration.

Inside the subfolders the input files are located to reproduce the simulation results from the thesis:
```
test/homogeneous/topopensystem/blackoil/So72_HomogeneousTopOpenSystem.input
test/homogeneous/topopensystem/blackoil/So74_HomogeneousTopOpenSystem.input

test/homogeneous/closedsystem/blackoil/So72_HomogeneousClosedSystem.input
test/homogeneous/closedsystem/blackoil/So72_HomogeneousClosedSystem_RsDivBy9.input
test/homogeneous/closedsystem/blackoil/So74_HomogeneousClosedSystem.input

test/homogeneous/bottominjection/blackoil/So72_HomogeneousBottomInjectionSystem.input
test/homogeneous/bottominjection/blackoil/So74_HomogeneousBottomInjectionSystem.input

test/fractures/bottominjection/blackoil/fracture_blackoil.input
test/fractures/bottominjection/blackoil/fracture_blackoil.input
test/fractures/bottominjection/blackoil/fracture_blackoil.input

```
If you want to execute a test with such an input file, use e.g.

```
./bottominjection_box So72_HomogeneousBottomInjectionSystem.input
```

Installation with Docker 
========================

Create a new folder in your favourite location and change into it

```bash
mkdir dumux
cd dumux
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/wendel2021a/-/raw/main/docker/docker_wendel2021a.sh
```

Open the Docker Container
```bash
bash docker_wendel2021a.sh open
```

After the script has run successfully, you may build all executables

```bash
cd wendel2021a/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following paths:
```
test/homogeneous/bottominjection/blackoil/
test/homogeneous/closedsystem/blackoil/
test/homogeneous/topopensystem/blackoil/
test/fractures/bottominjection/blackoil/
test/fractures/closedsystem/blackoil/
test/fractures/topopensystem/blackoil/
```

It can be executed with an input file, e.g.

```
cd test/homogeneous/bottominjection/blackoil
./bottominjection_box So72_HomogeneousBottomInjectionSystem.input
```
