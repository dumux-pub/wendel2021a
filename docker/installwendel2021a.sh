# dune-common
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.8
git reset --hard b2525d1490d9ae446eac3992b2a9382c15b1e4af
cd ..

# dune-geometry
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.8
git reset --hard f72861843533c001d3c12181b168c2ebdd4e69f0
cd ..

# dune-grid
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.8
git reset --hard bceb779ba617ca90e46a8be874c6f22177fcdf6b
cd ..

# dune-localfunctions
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.8
git reset --hard 83b2642da55830a02dd3a16de59ba72c448927cc
cd ..

# dune-istl
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.8
git reset --hard 59cc7182fa429598534db43383e5be284c6d40e8
cd ..

#dune-alugrid
git clone https://gitlab.dune-project.org/extensions/dune-alugrid
cd dune-alugrid
git checkout releases/2.8
git reset --hard ffd6cd15dc7092593c3d5827c2a2b43b81d7a32f
cd ..

#dune-foamgrid
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout releases/2.8
git reset --hard d346adbeb7534d7b3d7f60d2253772bd09248a7d
cd ..

# dumux
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git reset --hard c4472c0911bd615263a50650b902e650b776a854
cd ..


# installwendel2021a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/wendel2021a.git

./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
